package pl.net.nosal;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class TcpServer {
    private ServerSocket srvSocket;
    private Socket clntSocket;
    private PrintWriter out;
    private BufferedReader in;
    private boolean running;
    private String version = "1.0.0";
    private String creationDate="19.02.2021";
    private String srvCreationDate;
    public void start(int port) {
        try {
            srvSocket = new ServerSocket(port);
            clntSocket = srvSocket.accept();

            out = new PrintWriter(clntSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(clntSocket.getInputStream()));
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
            LocalDateTime now = LocalDateTime.now();
            srvCreationDate=dtf.format(now);
            running=true;
        }
         catch (Exception e){
            System.out.println("error");
        }
        while (running){
        try {
            //System.out.println(in.);
            String greeting = in.readLine();
            String jsonString;
            switch (greeting){
                case "uptime":
                    RuntimeMXBean rb = ManagementFactory.getRuntimeMXBean();
                    long uptime = rb.getUptime();
                    long hours = uptime/1000/60/60;
                    long temp = (uptime/1000)%60;
                    long minutes = temp/60;
                    temp = (temp%60);
                    long seconds = temp;
                    long mseconds = uptime - (hours*60*60*1000) - (minutes*60*1000)- (seconds*1000);
                    jsonString = new JSONObject()
                            .put("uptime", hours+"h:"+minutes+"m:"+seconds+"s:"+mseconds+"ms")
                            .toString();
                    out.println(jsonString);
                    break;
                case "info":
                    jsonString = new JSONObject()
                            .put("server_version", version)
                            .put("creation_date", creationDate)
                            .toString();
                    out.println(jsonString);
                    break;
                case "help":
                    jsonString = new JSONObject()
                            .put("uptime", "time server is up and running")
                            .put("info", "date of server cration and version of project")
                            .put("help", "showing possible commands")
                            .put("stop", "stops client and server")
                            .toString();
                    out.println(jsonString);
                    break;
                case "stop":
                    stop();
                    break;
                default:
                    out.println("use command \"help\" to show possible commands");
            }
        }catch (Exception e){
            System.out.println("error");
        }}
    }

    public void stop() {
        try{
            running=false;
        in.close();
        out.close();
        clntSocket.close();
        srvSocket.close();}catch (Exception e){
            System.out.println("error");}
    }

}
